const express = require('express');
const bodyParser = require("body-parser");
const cors = require("cors");
const cookieParser = require('cookie-parser')

const app = express();

var allowCrossDomain = function (req, res, next) {
  res.header('Access-Control-Allow-Origin', "*");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
}

app.use(allowCrossDomain);

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));


path = require('path');
fileUpload = require('express-fileupload');

app.set('views', __dirname + '/views');

// Middleware
app.set('view engine', 'ejs');
app.use(express.json());
app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));
app.use(fileUpload());

const db = require("./models");
const Role = db.role;


db.sequelize.sync({ force: true }).then(() => {
  console.log("Drop and re-sync db.");
  initial();
});

// app.get("/", (req, res) => {
//   res.render('welcome to Zoom Assistant')
// });

const { authJwt } = require("./middleware");

require('./routes/auth.routes')(app);
require('./routes/googleLogin.route')(app);
require('./routes/zoomLogin.route')(app);
require('./routes/fbLogin.route')(app);
require('./routes/forgetPassword.routes')(app);
require('./routes/changePassword.routes')(app);
require('./routes/profile.routes')(app);

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

function initial() {
  Role.create({
    id: 1,
    name: "user"
  });
}

process.on('SIGINT', async function () {
  console.log('SIGINT Shutdown received.');
  await db.sequelize.close();
  process.exit(0)
});

//Terminate active connection on kill
process.on('SIGTERM', async function () {
  console.log('SIGTERM Shutdown received.');
  await db.sequelize.close();
  process.exit(0)
});
