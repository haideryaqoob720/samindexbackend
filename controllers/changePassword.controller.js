const db = require("../models");
const User = db.user;
var bcrypt = require("bcryptjs");

exports.update = (req, res) => {
    if (req.body.password1 !== req.body.password2) {
        return res.json({ status: 'error', message: 'Passwords do not match. Please try again.' });
    }

    User.update({
        password: bcrypt.hashSync(req.body.password1, 8),
        // salt: newSalt
    }, {
        where: { id: req.body.userId }
    })
        .then(num => {
            res.send({
                message: "Password changed successfully."
            });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating password with id=" + id
            });
        });
};