var jwt = require("jsonwebtoken");
const db = require("../models");
const config = require("../config/auth.config");
const User = db.user;
const axios = require('axios');

module.exports = app => {

    app.post('/auth/facebook/callback', async (req, res) => {
        const { accessToken, userID } = req.body
        console.log("userId: ", userID);

        const { data } = await axios({
            url: 'https://graph.facebook.com/me',
            method: 'get',
            params: {
              fields: ['id', 'email', 'first_name', 'last_name'].join(','),
              access_token: accessToken,
            },
          });
          console.log(data); // { id, email, first_name, last_name }
          // Validation
        if(data.id == userID){
            // a valid user
            //check here if the user exists in DB, then login, else register and then login
            var email = data.email
            var firstName = data.first_name
            var lastName = data.last_name
            User.findAll({ where: { email } })
            .then(user => {

                if (user.length === 0) {
                    // Save User to Database
                    User.create({
                        firstName, lastName, email, roles: ["user"]
                    }).then((user) => {
                        var jwtToken = jwt.sign({ id: user.id }, config.secret, {
                            expiresIn: 86400 // 24 hours
                        });

                        console.log(`created user email${email} and token assigned is ${jwtToken}`);
                        res.status(200).send({
                            id: user.id, firstName, lastName, email, accessToken: jwtToken
                        });
                    }).catch(console.error)
                } else {
                    var jwtToken = jwt.sign({ id: user.id }, config.secret, {
                        expiresIn: 86400 // 24 hours
                    });
                    console.log(`Token ${jwtToken} assigned to ${email}`);
                    res.status(200).send({
                        id: user.id, firstName, lastName, email, accessToken: jwtToken
                    });
                }
            })
            .catch(err => {
                res.status(500).send({
                    message: " Error while assigning token"
                });
            });

        }else {
            console.log("fail")
            res.json({ status: 'error', data: 'failed to login'})
        }

    })
 
    app.get("/auth/facebook", (req, res) => {
        res.render('fbLogin')
    });

}