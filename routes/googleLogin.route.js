var jwt = require("jsonwebtoken");
const db = require("../models");
const config = require("../config/auth.config");
const User = db.user;
const Role = db.role;

// Google Auth
const { OAuth2Client } = require('google-auth-library');
const client = new OAuth2Client(process.env.GOOGLE_CLIENT_ID);

module.exports = app => {

    app.get("/auth/google", (req, res) => {
        res.render('googleLogin')
    });

    app.post("/auth/google", (req, res) => {
        let token = req.body.token;
        var email;
        async function verify() {
            const ticket = await client.verifyIdToken({
                idToken: token,
                audience: process.env.GOOGLE_CLIENT_ID
            });
            const payload = ticket.getPayload();
            const userid = payload['sub'];
            console.log('userid', userid);
            email = payload.email;
            var user = await User.findOne({ where: { email } });
            if (user == null) {
                // Save User to Database
                User.create({
                    firstName: payload.given_name,
                    lastName: payload.family_name,
                    email: payload.email,
                    roles: ["user"]
                }).then((user) => {
                    var jwtToken = jwt.sign({ id: user.id }, config.secret, {
                        expiresIn: 86400 // 24 hours
                    });

                    console.log('token and user data sent: ', { "userEmail": payload.email, jwtToken });
                    res.status(200).send({
                        id: user.id,
                        firstName: payload.given_name,
                        lastName: payload.family_name,
                        email: payload.email,
                        accessToken: jwtToken
                    });
                }).catch(console.error)
            } else {
                var jwtToken = jwt.sign({ id: user.id }, config.secret, {
                    expiresIn: 86400 // 24 hours
                });
                console.log('token and user data sent: ', { "userId": user.id, "userEmail": user.email, jwtToken });
                res.status(200).send({
                    id: user.id,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    email: user.email,
                    accessToken: jwtToken
                });
            }
        }
        verify()
            .catch(err => {
                res.status(500).send({ message: err.message });
            });

    });

    app.get("/logout", (req, res) => {
        // res.clearCookie('session-token');
        res.destroy(token);
        res.redirect('/googleLogin')
    });
}